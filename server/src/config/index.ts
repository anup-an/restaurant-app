import dotenv from 'dotenv';
dotenv.config();

const {PORT, DB_USER, DB_PASSWORD, DB_HOST, DB_NAME, DB_PORT, SALT_ROUNDS,JWT_SECRET_KEY,JWT_EXPIRATION_TIME} = process.env;

export default {
    PORT: PORT,
    DB: {
        USER: DB_USER,
        PASSWORD: DB_PASSWORD,
        HOST: DB_HOST,
        NAME: DB_NAME,
        PORT: DB_PORT 
    },
    SALT_ROUNDS: Number(SALT_ROUNDS),
    JWT_SECRET_KEY: JWT_SECRET_KEY,
    JWT_EXPIRATION_TIME: JWT_EXPIRATION_TIME
}
