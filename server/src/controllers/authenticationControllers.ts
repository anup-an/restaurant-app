import { Request, Response } from 'express';

export const loginUser = async (req:Request,res:Response): Promise<Response> => {
    return res.send('Log in user');
}

export const registerUser = async (req:Request,res:Response): Promise<Response> => {
    return res.send('Register user');
}