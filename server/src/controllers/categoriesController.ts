import { Request, Response } from 'express';


export const listAllCategories = async(req:Request,res:Response): Promise<Response> => {
    return res.send('List all categories');
}

export const findCategory = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Find a category by its id');
}

export const editCategory = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Edit a category by its id');
}

export const deleteCategory = async (req: Request, res: Response): Promise<Response> => {
    return res.send('Delete a category by its id');
}

export const createCategory = async (req:Request, res:Response): Promise<Response> => {
    return res.send('Create a new category');
}

export const listCategoryItems = async (req:Request, res:Response): Promise<Response> => {
    return res.send('List items belonging to a category');
}

