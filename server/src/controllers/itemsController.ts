import { Request, Response } from 'express';


export const listAllItems = async(req:Request,res:Response): Promise<Response> => {
    return res.send('List all items');
}

export const findItem = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Find an item by its id');
}

export const editItem = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Edit an item by its id');
}

export const deleteItem = async (req: Request, res: Response): Promise<Response> => {
    return res.send('Delete an item by its id');
}

export const createItem = async (req:Request, res:Response): Promise<Response> => {
    return res.send('Create a new item');
}