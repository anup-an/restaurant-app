import { Request, Response } from 'express';


export const listAllOrders = async(req:Request,res:Response): Promise<Response> => {
    return res.send('List all orders');
}

export const findOrder = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Find an ordew by its id');
}

export const editOrder = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Edit an order by its id');
}

export const deleteOrder = async (req: Request, res: Response): Promise<Response> => {
    return res.send('Delete an order by its id');
}

export const createOrder = async (req:Request, res:Response): Promise<Response> => {
    return res.send('Create a new order');
}

export const listOrderedItems = async (req:Request, res:Response): Promise<Response> => {
    return res.send('List ordered items belonging to an order');
}

