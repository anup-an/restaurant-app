import { Request, Response } from 'express';


export const findAllUsers = async(req:Request,res:Response): Promise<Response> => {
    return res.send('Find all users');
}

export const findUser = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Find a user by id');
}

export const editUser = async(req: Request, res: Response): Promise<Response> => {
    return res.send('Edit a user');
}

export const deleteUser = async (req: Request, res: Response): Promise<Response> => {
    return res.send('Delete a user');
}

export const saveUser = async (req:Request, res:Response): Promise<Response> => {
    return res.send('Save a user');
}