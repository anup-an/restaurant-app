import DB from '../config/pool';

const runQuery = async (tableName:string, queryString:string) => {
    try {
        const queryResult = await DB.query(queryString);
        console.log ('success', `Table "${tableName}" created successfully`);
        return queryResult;
    } catch (error) {
        console.log('error', `Table "${tableName}" not created`);
        return error.stack;
    }
}

const createUsers = async() => {
    const usersQuery = `DROP TABLE IF EXISTS "users" cascade;
    CREATE TABLE "users" (
        "id" SERIAL PRIMARY KEY,
        "email" varchar(100) UNIQUE NOT NULL,
        "first_name" varchar(100) NOT NULL,
        "last_name" varchar(100) NOT NULL,
        "address" varchar(250),
        "city" varchar(250),
        "postal_code" varchar(100),
        "phone_number" varchar(100),
        "created_at" timestamp NOT NULL
      );`
      return runQuery("users",usersQuery);
    
}

const createOrders = async() => {
    const ordersQuery = `DROP TABLE IF EXISTS "orders" cascade;

    CREATE TABLE "orders" (
      "id" SERIAL PRIMARY KEY,
      "code" uuid NOT NULL,
      "user_id" int NOT NULL,
      "order_status" varchar(100),
      "order_price" numeric(9,4)
    );
    ALTER TABLE "orders" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");`
    return runQuery("orders",ordersQuery);
}
  
const createOrderedItems = async () => {
    const orderedItemsQuery = `DROP TABLE IF EXISTS "ordered_items" cascade;

    CREATE TABLE "ordered_items" (
      "id" SERIAL PRIMARY KEY,
      "orders_id" int,
      "items_id" int
    );
    ALTER TABLE "ordered_items" ADD FOREIGN KEY ("orders_id") REFERENCES "orders" ("id");
  
    ALTER TABLE "ordered_items" ADD FOREIGN KEY ("items_id") REFERENCES "items" ("id");
  
    `
    return runQuery("ordered_items",orderedItemsQuery);
}  

const createItems = async () => {
    const itemsQuery = `DROP TABLE IF EXISTS "items" cascade;
    CREATE TABLE "items" (
      "id" SERIAL PRIMARY KEY,
      "title" varchar(200) NOT NULL,
      "slug" varchar(200) NOT NULL,
      "description" varchar(500) NOT NULL,
      "categories_id" int NOT NULL
    );
    ALTER TABLE "items" ADD FOREIGN KEY ("categories_id") REFERENCES "categories" ("id");
    `
    return runQuery("items",itemsQuery);;
}

const createCategories = async () => {
    const categoriesQuery = `DROP TABLE IF EXISTS "categories" cascade;    
    CREATE TABLE "categories" (
      "id" SERIAL PRIMARY KEY,
      "name" varchar(200) NOT NULL,
      "slug" varchar(200) NOT NULL
    );
    `
    return runQuery("categories", categoriesQuery);
}

Promise.all ([
    createUsers(),
    createItems(),
    createCategories(),
    createOrders(),
    createOrderedItems(),
]).then(() => {
    console.log('Database created');
})
.catch((err) => console.log(err));
  
  
  
  