import express, { Router } from 'express';
import cors from 'cors';
import routes from './routes';

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:false }))

app.get('/', (request,response) => {
    response.send('Hello from server');
})
app.use(routes());

app.listen (5000, () => {
    console.log('Listening to port 5000');
})
