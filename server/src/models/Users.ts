import { compare, genSaltSync, hashSync } from "bcrypt";
import config from '../config';
import pool from '../config/pool';

export default class User {
    email:string;
    password:string;
    first_name:string;
    last_name:string;
    address:string;
    city:string;
    postal_code:string;
    phone_number:string;
    created_at:string;
    constructor(
        email:string,
        password:string,
        first_name:string,
        last_name:string,
        address:string,
        city:string,
        postal_code:string,
        phone_number:string
    ){  
        this.email = email;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.address = address;
        this.city = city;
        this.postal_code = postal_code;
        this.phone_number = phone_number;
        this.created_at = Date();
        this.password =  hashSync(password, genSaltSync(config.SALT_ROUNDS));
    }
    comparePasswords = async (passwordDB:string, password:string):Promise<Boolean> => {
        const check = compare(password,passwordDB);
        return check;  
    }
    static findById = async (id:string):Promise<Response> => {
        const query = {
            name:'fetch-user-by-id',
            text:'SELECT * FROM users WHERE id = $1',
            values: [id]
        }
        try{
            const res = await pool.query(query);
            return res.rows[0];
        } catch (err) {
            return err.stack;
        }
    }
    static findByEmail = async (email:string,password:string):Promise<Response> => {
        const query = {
            name:'fetch-user-by-email',
            text:'SELECT * FROM users WHERE email = $2 AND password',
            values: [email,password]
        }
        try{
            const res = await pool.query(query);
            return res.rows[0];
        }catch (err) {
            return err.stack;
        }
    }    
}

