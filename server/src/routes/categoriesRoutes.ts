import { Router } from "express";
import { createCategory, listAllCategories, findCategory, editCategory, deleteCategory, listCategoryItems } from "../controllers/categoriesController";

const router = Router();

router.post('/', createCategory);
router.get('/', listAllCategories);
router.get('/:id', findCategory);
router.patch('/:id', editCategory);
router.delete('/:id', deleteCategory);
router.get('/:id/products', listCategoryItems);

export default router;
