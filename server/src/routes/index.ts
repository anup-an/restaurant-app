import { Router } from 'express';
import userRoutes from './userRoutes';
import ordersRoutes from './ordersRoutes';
import itemsRoutes from './itemsRoutes';
import categoriesRoutes from './categoriesRoutes';
import authenticationRoutes from './authenticationRoutes';


const routes = (): Router => {
    const router = Router();
    router.use('/api', authenticationRoutes)
    router.use('/api/users', userRoutes);
    router.use('/api/orders',ordersRoutes);
    router.use('/api/items', itemsRoutes);
    router.use('/api/categories', categoriesRoutes);
    return router;
}

export default routes;