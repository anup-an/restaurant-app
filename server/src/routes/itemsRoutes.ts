import { Router } from "express";
import { listAllItems, findItem, editItem, deleteItem, createItem } from "../controllers/itemsController";

const router = Router();

router.post('/', createItem);
router.get('/', listAllItems);
router.get('/:id', findItem);
router.patch('/:id', editItem);
router.delete('/:id', deleteItem);

export default router;