import { Router } from "express";
import { createOrder, listAllOrders, findOrder, editOrder, deleteOrder, listOrderedItems } from "../controllers/ordersController";

const router = Router();

router.post('/', createOrder);
router.get('/', listAllOrders);
router.get('/:id', findOrder);
router.patch('/:id', editOrder);
router.delete('/:id', deleteOrder);
router.get('/:id/items', listOrderedItems);


export default router;