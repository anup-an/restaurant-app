import { Router } from "express";
import { findAllUsers, findUser, editUser, deleteUser, saveUser } from "../controllers/userController";

const router = Router();

router.get('/', findAllUsers);
router.get('/:id', findUser);
router.patch('/:id', editUser);
router.delete('/:id', deleteUser);

export default router;